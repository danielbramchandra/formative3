/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author user
 */
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Formative2 {

    public static void main(String args[]) {
        int loop = 1;
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        while (loop <= 10) {
            try {
                String fileName = "form"+loop+".txt";
                FileInputStream fin = new FileInputStream(s+"/form1/"+fileName);
                byte[] strBytes = fin.readAllBytes();
                String str = new String(strBytes);
                System.out.println("File name : "+fileName);
                System.out.println("Content : "+str);
                System.out.println("");
                fin.close();
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                loop++;
            }
        }

    }
}
