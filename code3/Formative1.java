/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author user
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Formative1 {

    public static void main(String args[]) {
        int i = 1;
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();

        try {

//            System.out.println("Current relative path is: " + s);
            Path path = Paths.get(s + "/form1/");

            Files.createDirectories(path);
            
            System.out.println("Directory is created!");

            //Files.createDirectory(path);
        } catch (IOException e) {
            System.err.println("Failed to create directory!" + e.getMessage());
        }
        while (i <= 10) {
            FileOutputStream fout = null;
            File file = null;
            try {

                file = new File(s+"/form1/form" + i + ".txt");

                fout = new FileOutputStream(file);
                String text = "Hello java " + i;
                if (!file.exists()) {
                    file.createNewFile();
                }

                byte[] b = text.getBytes();
              
                fout.write(b);
                fout.close();
                System.out.println("File Created");
            } catch (Exception e) {
               System.err.println("Failed to create file!" + e.getMessage());
            } finally {

                i++;
            }
        }

    }
}
